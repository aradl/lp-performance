# std
import os, sys
import math
import datetime as dt

# pip
import configargparse
import yaml
import pandas as pd
import numpy as np

from pycoingecko import CoinGeckoAPI
#from solana.rpc.api import Client

from bokeh.plotting import figure, output_file, save
from bokeh.models import Range1d, Text, Label, Label
from bokeh.layouts import layout
from bokeh.io import output_file, curdoc

def plot_farm(farm):
    # build dict
    c_names = []
    for c in farm['coins']:
        c['market'], c['ohlc'] = get_market_info(c['ticker'])
        c_names.append(c['ticker'])
    farm = calc_farm_info(farm)

    # setup plots
    #curdoc().theme = "dark_minimal"
    output_file(f"{c_names[0]}-{c_names[1]}_LP.html")

    # capture plots
    candle_p = []
    for c in farm['coins']:
        candle_p.append(get_candle_figure(c, farm['entry_dt']))

    perf_p = get_perf_figure(farm)
    corr_p = get_corr_figure(farm)

    # set layout
    l = layout([
            [perf_p, corr_p],
            candle_p])
    
    # show the results
    save(l)

def get_corr_figure(farm):
    info_df  = farm['info_df']
    entry_dt = farm['entry_dt']

    hist, edges = np.histogram(info_df['corr'].dropna())

    TOOLS = "pan,box_zoom,reset,save"

    p = figure(tools=TOOLS, plot_width=500, title = f"Market Correlation")
    p.quad(top=hist, bottom=0, left=edges[:-1], right=edges[1:],
            fill_color="navy", line_color="white", alpha=0.5)

    p.x_range = Range1d(-1, 1)

    return p

def get_perf_figure(farm):
    info_df  = farm['info_df']
    entry_dt = farm['entry_dt']
    current_beven_APY = farm['current_beven_APY']

    #TODO add HoverTool
    TOOLS = "crosshair,pan,box_zoom,reset,save"

    c_names = [cn for cn in info_df.columns if '_price' in cn]
    p = figure(x_axis_type="datetime", tools=TOOLS, plot_width=1500, title = f"LP Performance")

    # add performance information
    p.line(info_df['date'], info_df['imp_loss'], legend_label=f"Impermanent Loss", line_width=2, line_color="red")
    p.line(info_df['date'], info_df['performance'], legend_label=f"Hodl Performance", line_width=2, line_color="orange")
    p.line(info_df['date'], info_df['total_perf'], legend_label=f"Performance", line_width=2, line_color="green")

    # add marker for LP entry point
    top    = np.amax([info_df['imp_loss'].max(), info_df['performance'].max(), info_df['total_perf'].max()])
    bottom = np.amin([info_df['imp_loss'].min(), info_df['performance'].min(), info_df['total_perf'].min()])
    p.line([entry_dt, entry_dt], [bottom, top], line_width=2, line_color="indigo", legend_label="Entry Point")

    # add performance stats
    apy = np.around(current_beven_APY * -100, 2)

    citation = Label(x=entry_dt, y=top/2, x_units='data', y_units='data',
                     text=f"Break Even APY: {apy}%", render_mode='css',
                     border_line_color='black', border_line_alpha=1.0,
                     background_fill_color='white', background_fill_alpha=1.0)
    p.add_layout(citation)

    p.legend.location = "top_left"
    p.legend.border_line_color = "black"

    return p


def get_candle_figure(coin, entry_dt=None):
    ohlc   = coin['ohlc']
    market = coin['market']
    ticker = coin['ticker']

    inc = ohlc.close > ohlc.open
    dec = ohlc.open > ohlc.close
    w = 4*24*60*60*1000 # 4 days in ms

    TOOLS = "pan,box_zoom,reset,save"

    p = figure(x_axis_type="datetime", tools=TOOLS, plot_width=1000, title = f"{ticker} Candle/Price")
    p.xaxis.major_label_orientation = math.pi/4
    p.grid.grid_line_alpha=0.3

    p.segment(ohlc.date, ohlc.high, ohlc.date, ohlc.low, color="black")
    p.vbar(ohlc.date[inc], w, ohlc.open[inc], 
            ohlc.close[inc], fill_color="#D5E1DD", line_color="black")

    p.vbar(ohlc.date[dec], w, ohlc.open[dec], 
            ohlc.close[dec], fill_color="#F2583E", line_color="black")

    # hourly market data
    p.line(market['date'], market['prices'], line_width=1)

    # add marker for LP entry point
    top    = market['prices'].max()
    bottom = market['prices'].min()
    p.line([entry_dt, entry_dt], [bottom, top], line_width=2, line_color="indigo")

    return p


def calc_farm_info(farm):
    # combine data
    comb_df = None
    c_names = []
    for c in farm['coins']:
        c_name = f"{c['ticker']}_price"
        c_names.append(c_name)

        if(comb_df is None):
            mk_series = c['market']['prices']
            comb_df = pd.DataFrame(data=mk_series.to_numpy(),
                                    columns=[c_name], 
                                    index=mk_series.index)
            comb_df['date'] = c['market']['date']
        else:
            comb_df[c_name] = c['market']['prices']

    # calculate corrilation between data
    # market data is in hours
    comb_df['corr'] = comb_df[c_names[0]].rolling(24).corr(
                                comb_df[c_names[1]])

    comb_df['corr'] = comb_df['corr'].rolling(24).mean()

    # fix entry point to 60 days ago
    latest_dt  = comb_df['date'].iloc[-1].to_pydatetime()
    entry_dt  = latest_dt - dt.timedelta(days=farm['entry_days_ago'])
    entry_dt  = min(comb_df['date'].to_list(), 
                        key=lambda sub: abs(sub - entry_dt))
    entry_idx = comb_df[comb_df['date'] == entry_dt].index[0]

    farm['entry_dt'] = entry_dt

    # fix purchase amount
    pa = comb_df.loc[entry_idx, c_names[0]] 
    pb = comb_df.loc[entry_idx, c_names[1]] 
    r1 = pa/pb

    # calculate imperment loss overtime
    comb_df['imp_loss'] = comb_df.apply(lambda row : imp_loss(r1, 
                                         (row[c_names[0]] / row[c_names[1]])
                                         ), axis = 1)
    comb_df.loc[:entry_idx, 'imp_loss'] = np.nan
    
    # calculate hodling performance overtime
    comb_df['performance'] = comb_df.apply(lambda row : performance(pa, pb, 
                                            row[c_names[0]], row[c_names[1]]
                                            ), axis = 1)
    comb_df.loc[:entry_idx, 'performance'] = np.nan

    # set final performance
    comb_df['total_perf'] = comb_df['performance'] + comb_df['imp_loss']

    # calculate APY requiered to break even
    #TODO check
    # get average hourly change in imp loss - convert to day - convert to year
    farm['current_beven_APY'] = np.nanmean(comb_df['imp_loss'].diff()) * 24 * 365


    farm['info_df']  = comb_df
    return farm


def imp_loss(r1, r2):
    # https://chainbulletin.com/impermanent-loss-explained-with-examples-math/
    #TODO check
    p  = r1/r2
    il = (2 * (math.sqrt(p) / (p + 1))) - 1

    return il

def performance(pa1, pb1, pa2, pb2):
    #NOTE check it
    usd = 10
    ca = usd / pa1
    cb = usd / pb1

    pa = ((ca*pa2) - (ca*pa1)) / (ca*pa1)
    pb = ((cb*pb2) - (cb*pb1)) / (cb*pb1)

    return pa + pb


def get_market_info(ticker='btc'):
    cg = CoinGeckoAPI()

    print(f'getting data for {ticker}... ', end = '')

    # convert ticker to id
    #NOTE this can be slow sometimes
    #TODO save this off to file to save on API calls
    coin_list = cg.get_coins_list()
    cg_id = None
    for coin in coin_list:
        if(coin['symbol'] == ticker.lower()):
            cg_id = coin['id']
            break

    # get candle information
    data = cg.get_coin_ohlc_by_id(id=cg_id, vs_currency='usd', days=90)
    ohlc = pd.DataFrame(data, columns = ['time','open','high','low','close'])

    ohlc['date'] = pd.to_datetime(ohlc['time'], unit='ms')

    # get higher precision market data
    data = cg.get_coin_market_chart_by_id(id=cg_id, 
                                            vs_currency='usd', 
                                            days=90)

    # convert to df
    market = None
    for key, value in data.items():
        if(market is None):
            market = pd.DataFrame(value, columns=['time', key])
        else:
            ndf    = pd.DataFrame(value, columns=[f'{key}_time', key])
            market = pd.concat([market, ndf], axis=1)

    # organize df
    market = market.drop(columns=['market_caps_time', 'total_volumes_time'])
    market['date'] = pd.to_datetime(market['time'], unit='ms')
    print('done')

    return market, ohlc

#def sol_test():
#    """learn how to interact with on-chain information"""
#    sol_public_address = ''
#    http_client = Client("https://api.devnet.solana.com")
#    print(http_client)

def parse_args():
    """Setup and read in arguments for script"""
    p = configargparse.ArgParser(
                default_config_files=['./farm.conf'],
                config_file_parser_class=configargparse.ConfigparserConfigFileParser,
            )

    # [Farm]
    p.add('--farm', type=yaml.safe_load) 

    """
    to keep track of farm structure
    farm = {'coins': 
                    [{'ticker': 'SLRS',
                      'weight': 0.5,
                      'ohlc': None,
                      'market': None,
                     },
                     {'ticker': 'USDC',
                      'weight': 0.5,
                      'ohlc': None,
                      'market': None,
                     },
                    ],
            'location': None,
            'entry_dt': None,
            'info_df': None,
            'break_even_APY': None,
            }
    """
    # parse
    return p.parse_args()


def main():
    # setup
    args = parse_args()
    plot_farm(args.farm)


# run the main function
if __name__ == '__main__':
    main()

