# LP-performance

DeFi farm LP price performance with live market data in a time-series format.

I found most impermanent loss calculators missing a few critical parts to aid in my understanding of risk for specific LP pools.
This code is setup to plug in the coin ticker names for the LP pair and see historical performance/loss and market correlation in one view.
It also calculates a projected break-even APY to help understand the fee activity needed to break even.
This is done by averaging the historic hourly impermanent loss delta (hourly break-even APR) and multiplying out to a year.

***NOTE: calculations need a second option***

## Setup

1. Install Python, at least version 3.8
1. Install required packages with `pip install -r ./requierments.txt`
1. Set parameter in the config file `/src/farm.conf`
1. Run with `python main.py`
1. View plots with browser i.e. `firefox *.html`

### Configuration File Setup

Setup the farm dictionary with the ticker names for the coins of interest and their respective weight. (only pairs are supported at the moment) 
For historic performance calculations enter the amount of days ago, from todays date, that you entered the farm. (only 0 to 90 days are supported at the moment)

### Example Output
![main.html](/docs/plot_example.png)

## Donate
Wallets:
- BTC: `bc1q2l5rxc6nvq4s9rrd053j95dlsn090uvrejf0qp`
- SOL: `6a583ZAxWUpjJbe1XQkzJHRbw9jcN4eSAkANcDKK5bwE`

## Contact
I am always open to suggestions and bug fixes.

<aradl.dev@pm.me>

## In Progress
- flexible timescales (right now it's fixed to past 90 days)
- more performance numbers

## Future Updates
- automatically pull LP positions on chain
    - if anyone knows how to do this I am listening...
- support more than 2 tokens in LP
